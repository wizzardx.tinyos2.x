"""

Unit tests for tos.py, written in Behavior Driven Development style
(http://behaviour-driven.org/BehaviourDrivenDevelopment).

These tests should be run with the nosetests utility

"""

import tos
from nose.tools import assert_raises
import socket
import serial

# tos protocol byte values, copied from tos.py:
HDLC_FLAG_BYTE = 0x7e
HDLC_CTLESC_BYTE = 0x7d

TOS_SERIAL_ACTIVE_MESSAGE_ID = 0
TOS_SERIAL_CC1000_ID = 1
TOS_SERIAL_802_15_4_ID = 2
TOS_SERIAL_UNKNOWN_ID = 255

SERIAL_PROTO_ACK = 67
SERIAL_PROTO_PACKET_ACK = 68
SERIAL_PROTO_PACKET_NOACK = 69
SERIAL_PROTO_PACKET_UNKNOWN = 255


class FMReqPacket(tos.Packet):
    """A packet structure, borrowed from tos-deluge.py for testing"""
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('cmd',    'int',  1),
                            ('imgNum', 'int',  1),
                            ('offset', 'int',  2),
                            ('length', 'int',  2),
                            ('data',   'blob', None)],
                            packet)


class MockSerialModule:
    """A class that subsitutes for the serial module imported by tos"""
    read_byte_queue = []
    written_bytes = [] # List of bytes written to serial
    read_delay = 0 # Sleep this long before returning bytes
    class Serial:
        def __init__(self, port, baudrate, rtscts, timeout):
            self.timeout = timeout
            self._open = True

        def close(self):
            self._open = False

        def flushInput(self):
            # Fail if not open
            if not self._open:
                raise serial.SerialException

        def read(self):
            # Fail if not open
            if not self._open:
                raise serial.SerialException

            # Raise a socket timeout exception if the read delay is too long:
            if (self.timeout is not None and
                MockSerialModule.read_delay > self.timeout):
                raise socket.timeout

            # Sleep and return a byte:
            simtime.sleep(MockSerialModule.read_delay)
            byte = MockSerialModule.read_byte_queue.pop(0)
            return chr(byte)

        def write(self, byte):
            # Fail if not open
            if not self._open:
                raise serial.SerialException
            MockSerialModule.written_bytes.append(ord(byte))

class MockTimeModule:
    """Fake time object that gets used instead of the real 'time' module.

    MockTimeModule simulates time passing, so that tests don't have to waste
    'real' time to test timeout logic."""

    def __init__(self):
        self._time = 0
        self.auto_advance = 0 # How many simulated seconds should automatically
                              # advance between calls to time()

    def time(self):
        """Return current simulated time"""
        self._time += self.auto_advance
        return self._time

    def sleep(self, seconds):
        """Advance simulated time by the requested number of seconds"""
        self._time += seconds


class MockSerial:
    """A class that gets substituted for tos.Serial"""
    def __init__(self, port, baudrate, flush=False, debug=False, qsize=10,
                 timeout=None):
        self.write_history = []

    def write(self, payload, timeout=None):
        self.write_history.append((payload, timeout))
        return True


def setup():
    """Setup logic to run before all the tests"""

    # Setup simulated time:
    global simtime
    simtime = MockTimeModule()

    # Replace tos.py's imported modules with our mock versions:
    global _tos_serial_bkp, _tos_time_bkp
    _tos_serial_bkp = tos.serial
    _tos_time_bkp = tos.time
    tos.serial = MockSerialModule
    tos.time = simtime

def teardown():
    """Teardown logic to run afer all the tests"""
    tos.serial = _tos_serial_bkp
    tos.time = _tos_time_bkp


class TestSimpleSerialBehaviour:
    """Tests for tos.SimpleSerial behavior"""
    def setup(self):
        self.ss_no_timeout = tos.SimpleSerial('/dev/ttyUSB0', 115200)
        self.ss_timeout = tos.SimpleSerial('/dev/ttyUSB0', 115200, timeout=0.1)
        MockSerialModule.read_delay = 0
        MockSerialModule.read_byte_queue = []
        MockSerialModule.written_bytes = []
        simtime.auto_advance = 0

    # SimpleSerial.close() tests

    def test_write_should_fail_after_close(self):
        # Test writing before and after closing
        def _test_write():
            q = MockSerialModule.read_byte_queue
            q += [HDLC_FLAG_BYTE, SERIAL_PROTO_ACK, 123, 0x63, 0x97,
                HDLC_FLAG_BYTE]
            payload = tos.ActiveMessage()
            self.ss_no_timeout.write(payload, seqno=123)
        _test_write()
        self.ss_no_timeout.close()
        assert_raises(serial.SerialException, _test_write)

    # SimpleSerial.read() tests

    def test_read_should_work(self):
        MockSerialModule.read_byte_queue = [HDLC_FLAG_BYTE, 1, 2, 3, 49, 97,
                                            HDLC_FLAG_BYTE]
        packet = self.ss_no_timeout.read()
        assert packet.data == [1, 2, 3]

    def test_read_should_fail_for_bad_crc(self):
        MockSerialModule.read_byte_queue = [HDLC_FLAG_BYTE, 1, 2, 4, 49, 97,
                                            HDLC_FLAG_BYTE]
        assert_raises(tos.ReadCRCError, self.ss_no_timeout._read)

    def test_read_should_skip_earlier_partial_packet(self):
        MockSerialModule.read_byte_queue = [100, 101, 102, HDLC_FLAG_BYTE,
                                            HDLC_FLAG_BYTE, 1, 2, 3, 49, 97,
                                            HDLC_FLAG_BYTE]
        packet = self.ss_no_timeout.read()
        assert packet.data == [1, 2, 3]

    def test_read_should_fail_for_bad_crc_after_earlier_partial_packet(self):
        MockSerialModule.read_byte_queue = [100, 101, 102, HDLC_FLAG_BYTE,
                                            HDLC_FLAG_BYTE, 1, 2, 4, 49, 97,
                                            HDLC_FLAG_BYTE]
        assert_raises(tos.ReadCRCError, self.ss_no_timeout.read)

    def test_read_should_correctly_decode_escaped_packets(self):
        MockSerialModule.read_byte_queue = [HDLC_FLAG_BYTE, 1, 2, 3,
                                            HDLC_CTLESC_BYTE, 94, 0xde, 0xd2,
                                            HDLC_FLAG_BYTE]
        packet = self.ss_no_timeout.read()
        assert packet.data == [1, 2, 3, HDLC_FLAG_BYTE]

    def test_read_longer_than_timeout_should_fail(self):
        MockSerialModule.read_byte_queue = [HDLC_FLAG_BYTE, 1, 2, 3, 49, 97,
                                            HDLC_FLAG_BYTE]
        MockSerialModule.read_delay = 0.2
        assert_raises(tos.ReadTimeoutError, self.ss_no_timeout.read, 0.1)

    def test_read_with_timeout_should_work(self):
        MockSerialModule.read_byte_queue = [HDLC_FLAG_BYTE, 1, 2, 3, 49, 97,
                                            HDLC_FLAG_BYTE]
        packet = self.ss_no_timeout.read(timeout=0.2)
        assert packet.data == [1, 2, 3]

    def test_read_without_timeout_should_work(self):
        MockSerialModule.read_byte_queue = [HDLC_FLAG_BYTE, 1, 2, 3, 49, 97,
                                            HDLC_FLAG_BYTE]
        packet = self.ss_no_timeout.read()
        assert packet.data == [1, 2, 3]

    def test_read_should_default_to_init_timeout(self):
        MockSerialModule.read_byte_queue = [HDLC_FLAG_BYTE, 1, 2, 3, 49, 97,
                                            HDLC_FLAG_BYTE]
        MockSerialModule.read_delay = 0.2
        assert_raises(tos.ReadTimeoutError, self.ss_timeout.read)

    def test_read_should_filter_acks(self):
        q = MockSerialModule.read_byte_queue
        q += [HDLC_FLAG_BYTE, SERIAL_PROTO_ACK, 82, 0x28, 0x22, HDLC_FLAG_BYTE]
        assert self.ss_no_timeout.read() is None

    # SimpleSerial.write() tests

    def test_write_should_work(self):
        # Add a write ack to the read buffer:
        q = MockSerialModule.read_byte_queue
        q += [HDLC_FLAG_BYTE, SERIAL_PROTO_ACK, 123, 0x63, 0x97,
              HDLC_FLAG_BYTE]
        payload = tos.ActiveMessage()
        self.ss_no_timeout.write(payload, seqno=123)

    def test_write_should_send_expected_bytes_to_serial(self):
        q = MockSerialModule.read_byte_queue
        q += [HDLC_FLAG_BYTE, SERIAL_PROTO_ACK, 82, 0x28, 0x22, HDLC_FLAG_BYTE]

        packet = FMReqPacket((10, 20, 30, 5, [101, 102, 103, 104, 105]))
        am_packet = tos.ActiveMessage(packet, amid=0xAB)
        self.ss_no_timeout.write(am_packet, seqno=82)
        assert MockSerialModule.written_bytes == [126, 68, 82, 0, 255, 255, 0,
                                                  0, 11, 0, 171, 10, 20, 0, 30,
                                                  0, 5, 101, 102, 103, 104,
                                                  105, 205, 198, 126]

        # Explanation of written bytes:
        # 126 - HDLC_FLAG_BYTE
        #
        # DataFrame
        # 68  - DataFrame.protocol (SERIAL_PROTO_PACKET_ACK)
        # 82  - DataFrame.seqno
        # 0   - DataFrame.dispatch
        #
        # DataFrame.data (ActiveMessage)
        # 255 - ActiveMessage.destination byte 1/2
        # 255 - ActiveMessage.destination byte 2/2
        # 0   - ActiveMessage.source byte 1/2
        # 0   - ActiveMessage.source byte 2/2
        # 11  - ActiveMessage.group byte 1/2
        # 0   - ActiveMessage.group byte 2/2
        # 171 - ActiveMessage.type
        #
        # ActiveMessage.data (FMReqPacket)
        # 10  - FMReqPacket.cmd
        # 20  - FMReqPacket.imgNum
        # 0   - FMReqPacket.offset byte 1/2
        # 30  - FMReqPacket.offset byte 2/2
        # 0   - FMReqPacket.length byte 1/2
        # 5   - FMReqPacket.length byte 2/2
        # 101 - FMReqPacket.blob byte 1/5
        # 102 - FMReqPacket.blob byte 2/5
        # 103 - FMReqPacket.blob byte 3/5
        # 104 - FMReqPacket.blob byte 4/5
        # 105 - FMReqPacket.blob byte 5/5
        #
        # 205 - CRC byte 1/2
        # 198 - CRC byte 2/2
        # 126 - HDLC_FLAG_BYTE

    def test_write_should_time_out_when_acks_take_too_long(self):
        q = MockSerialModule.read_byte_queue
        q += [HDLC_FLAG_BYTE, SERIAL_PROTO_ACK, 82, 0x28, 0x22, HDLC_FLAG_BYTE]
        MockSerialModule.read_delay = 0.2
        packet = FMReqPacket((10, 20, 30, 5, [101, 102, 103, 104, 105]))
        am_packet = tos.ActiveMessage(packet, amid=0xAB)
        assert_raises(tos.WriteTimeoutError, self.ss_no_timeout.write,
                      am_packet, seqno=82, timeout=0.1)

    def test_write_should_fail_for_incorrect_ack_sequence_numbers(self):
        q = MockSerialModule.read_byte_queue
        q += [HDLC_FLAG_BYTE, SERIAL_PROTO_ACK, 82, 0x28, 0x22, HDLC_FLAG_BYTE]
        packet = FMReqPacket((10, 20, 30, 5, [101, 102, 103, 104, 105]))
        am_packet = tos.ActiveMessage(packet, amid=0xAB)
        assert_raises(tos.BadAckSeqnoError, self.ss_no_timeout.write,
                      am_packet, seqno=81)

    def test_write_should_buffer_data_read_while_waiting_for_ack(self):
        q = MockSerialModule.read_byte_queue
        q += [HDLC_FLAG_BYTE, 1, 2, 3, 49, 97, HDLC_FLAG_BYTE]
        q += [HDLC_FLAG_BYTE, 5, 6, 7, 0xb1, 0x31, HDLC_FLAG_BYTE]
        q += [HDLC_FLAG_BYTE, SERIAL_PROTO_ACK, 82, 0x28, 0x22, HDLC_FLAG_BYTE]
        packet = FMReqPacket((10, 20, 30, 5, [101, 102, 103, 104, 105]))
        am_packet = tos.ActiveMessage(packet, amid=0xAB)
        self.ss_no_timeout.write(am_packet, seqno=82)
        packet = self.ss_no_timeout.read()
        assert packet.data == [1, 2, 3]
        packet = self.ss_no_timeout.read()
        assert packet.data == [5, 6, 7]

    # SimpleSerial.write() regression tests

    def test_write_should_correctly_handle_late_acks(self):
        """When writes time out, their ack packets may still be received later.
        tos.SimpleSerial.write() needs to handle this correctly, instead of
        throwing a BadAckSeqnoError exception."""

        # Push 2 acks into the read buffer:
        q = MockSerialModule.read_byte_queue
        q += [HDLC_FLAG_BYTE, SERIAL_PROTO_ACK, 82, 0x28, 0x22, HDLC_FLAG_BYTE]
        q += [HDLC_FLAG_BYTE, SERIAL_PROTO_ACK, 83, 0x09, 0x32, HDLC_FLAG_BYTE]

        # Setup a packet for testing:
        packet = FMReqPacket((10, 20, 30, 5, [101, 102, 103, 104, 105]))
        am_packet = tos.ActiveMessage(packet, amid=0xAB)

        # Simulate a timeout for the first write:
        MockSerialModule.read_delay = 1
        assert_raises(tos.WriteTimeoutError, self.ss_timeout.write, am_packet,
                      seqno=82)

        # Now read the next packet (there should be no ack error)
        MockSerialModule.read_delay = 0
        self.ss_timeout.write(am_packet, seqno=83, timeout=0.1)

    # SimpleSerial.add_received_packet_filter() tests

    def test_received_packet_filters_should_work(self):
        callback_count = [0]
        def _callback_func(packet):
            callback_count[0] += 1
        test_data = [HDLC_FLAG_BYTE, 1, 2, 3, 49, 97, HDLC_FLAG_BYTE]
        q = MockSerialModule.read_byte_queue

        # Test before adding the filter func
        q += test_data
        packet = self.ss_no_timeout.read()
        assert packet.data == [1, 2, 3]

        # Test after adding the filter func
        self.ss_no_timeout.add_received_packet_filter(_callback_func)
        q += test_data
        packet = self.ss_no_timeout.read()
        assert packet is None
        assert callback_count[0] == 1

        # Test after removing the filter func
        self.ss_no_timeout.remove_received_packet_filter(_callback_func)
        q += test_data
        packet = self.ss_no_timeout.read()
        assert packet.data == [1, 2, 3]
        assert callback_count == [1]

    # SimpleSerial.timeout attribute tests

    def test_timeout_attribute_should_work(self):
        start_time = simtime.time()
        MockSerialModule.read_byte_queue = [HDLC_FLAG_BYTE, 1, 2, 3, 49, 97,
                                            HDLC_FLAG_BYTE]
        MockSerialModule.read_delay = 0.01
        ss = tos.SimpleSerial('/dev/ttyUSB0', 115200, timeout=0.005)
        assert ss.timeout == 0.005
        assert_raises(tos.ReadTimeoutError, ss.read)
        ss.timeout = 0.015
        assert ss.timeout == 0.015
        ss.read()

class TestSeqTrackerBehavior:
    """Tests for tos.SeqTracker behavior"""

    # SeqTracker.seq_sent() tests

    def test_should_keep_seqnos_for_the_minium_required_time(self):
        t = tos.SeqTracker(0.01)
        t.seqno_sent(123)
        simtime.sleep(0.008)
        assert 123 in t

    def test_should_not_keep_seqnos_too_long(self):
        t = tos.SeqTracker(0.01)
        t.seqno_sent(123)
        simtime.sleep(0.012)
        assert not 123 in t

    def test_should_work_correctly_over_interval(self):
        # Should work correctly over a period of time. ie, ensure that the
        # internal timing logic of SeqTracker works correctly
        _start_time = [simtime.time()]

        KEEP_FOR = 0.1 # How long to remember unacknowledged sequence numbers
        SMALL_DIFF = 0.02 # Used to get times just before or after an
                          # interesting event takes place
        t = tos.SeqTracker(KEEP_FOR)

        def _sleep_until(time_since_start):
            now = simtime.time()
            sleep_until = _start_time[0] + time_since_start
            # Requested 'sleep until' time should be in the future:
            assert sleep_until > now
            # Sleep until the requested time comes:
            simtime.sleep(sleep_until - now)

        # Insert first seqno, and wait until just before it disappears:
        t.seqno_sent(100)
        _sleep_until(KEEP_FOR - SMALL_DIFF)
        assert 100 in t

        # Insert the next sequence number, and wait until just after the 1st
        # seqno disappears
        t.seqno_sent(200)
        _sleep_until(KEEP_FOR + SMALL_DIFF)

        # 1st seqno should be gone, and the first still there:
        assert 100 not in t
        assert 200 in t

        # Sleep until just before 2nd seqno should disappear:
        _sleep_until(KEEP_FOR - SMALL_DIFF + KEEP_FOR - SMALL_DIFF)

        # 2nd seqno should still be there:
        assert 200 in t

        # Sleep until just after 2nd seqno should disappear:
        _sleep_until(KEEP_FOR - SMALL_DIFF + KEEP_FOR + SMALL_DIFF)
        assert 200 not in t

    # WriteAckTracker.seq_acked() tests

    def test_seq_acked_should_succeed_if_seqno_was_sent(self):
        t = tos.SeqTracker(0.1)
        t.seqno_sent(999)
        t.seqno_acked(999)

    def test_seq_acked_should_fail_if_seqno_was_not_sent(self):
        t = tos.SeqTracker(0.1)
        t.seqno_sent(999)
        assert_raises(tos.BadAckSeqnoError, t.seqno_acked, 100)

    def test_ack_removes_oldest_queued_seqno(self):
        # Put the same seqno in the queue a few times, with the
        # 1st element being before the others, chronologically
        t = tos.SeqTracker(0.1)
        t.seqno_sent(100)
        simtime.sleep(0.01)
        oldest_sent_before = simtime.time()
        t.seqno_sent(100)
        t.seqno_sent(100)

        # Ack the seq, then check that the remaining sequences are newer:
        t.seqno_acked(100)
        times = t.get_seqno_sent_times(100)
        assert len(times) == 2
        for sent_time in times:
            assert sent_time >= oldest_sent_before

    def test_ack_should_fail_if_seqno_sent_too_long_ago(self):
        t = tos.SeqTracker(0.01)
        t.seqno_sent(999)
        simtime.sleep(0.02)
        assert_raises(tos.BadAckSeqnoError, t.seqno_acked, 999)

    def test_should_work_for_multiple_sends_and_acks(self):
        t = tos.SeqTracker(0.01)
        t.seqno_sent(1)
        t.seqno_sent(2)
        t.seqno_sent(3)
        t.seqno_acked(3)
        t.seqno_acked(2)
        t.seqno_acked(1)

    def test_should_fail_if_seqno_acked_more_times_than_sent(self):
        t = tos.SeqTracker(0.01)
        t.seqno_sent(1)
        t.seqno_sent(2)
        t.seqno_sent(3)
        t.seqno_acked(3)
        assert_raises(tos.BadAckSeqnoError, t.seqno_acked, 3)
        t.seqno_acked(2)
        assert_raises(tos.BadAckSeqnoError, t.seqno_acked, 2)
        t.seqno_acked(1)
        assert_raises(tos.BadAckSeqnoError, t.seqno_acked, 1)

    # WriteAckTracker.get_seqno_sent_times() tests

    def test_get_seqno_sent_times_should_work_correctly(self):
        t = tos.SeqTracker(1)
        time_before = simtime.time()
        simtime.sleep(0.01)
        t.seqno_sent(1)
        t.seqno_sent(2)
        t.seqno_sent(2)
        t.seqno_sent(3)
        simtime.sleep(0.01)
        time_after = simtime.time()

        sent_times = t.get_seqno_sent_times(2)
        assert len(sent_times) == 2
        for sent_time in sent_times:
            assert time_before < sent_time < time_after

class TestSerialBehaviour:
    """Tests for tos.Serial behavior"""
    def setup(self):
        self.s_no_timeout = tos.Serial('/dev/ttyUSB0', 115200)
        self.s_timeout = tos.Serial('/dev/ttyUSB0', 115200, timeout=0.1)
        MockSerialModule.read_delay = 0
        MockSerialModule.read_byte_queue = []
        MockSerialModule.written_bytes = []
        simtime.auto_advance = 0

    # Serial.close() tests

    def test_write_should_fail_after_close(self):
        # Test writing before and after closing
        def _test_write():
            # Add an ack for seqno 1 to the read queue (not correct for the
            # 2nd write test, but the SerialException gets thrown before
            # the 2nd ack gets read).
            q = MockSerialModule.read_byte_queue
            q += [HDLC_FLAG_BYTE, SERIAL_PROTO_ACK, 1, 0xbe, 0x48,
                HDLC_FLAG_BYTE]
            payload = tos.ActiveMessage()
            self.s_no_timeout.write(payload)

        _test_write()
        self.s_no_timeout.close()
        assert_raises(serial.SerialException, _test_write)


    # SimpleSerial.timeout attribute tests

    def test_timeout_attribute_should_work(self):
        MockSerialModule.read_byte_queue = [HDLC_FLAG_BYTE, 1, 2, 3, 49, 97,
                                            HDLC_FLAG_BYTE]
        MockSerialModule.read_delay = 1
        s = tos.Serial('/dev/ttyUSB0', 115200, timeout=0.5)
        assert s.timeout == 0.5
        simtime.auto_advance = 0.1
        assert_raises(tos.ReadTimeoutError, s.read)
        s.timeout = 2.0
        assert s.timeout == 2.0
        s.read()

    # Regression tests
    # TODO: should not increment sequence number between write retries
    # TODO: test_read_should_return_true

class TestAMBehaviour:
    """Tests for tos.AM behavior"""
    def setup(self):
        """Prepare for a single AM behaviour test"""
        self.s = MockSerial('/dev/ttyUSB0', 115200)
        self.am = tos.AM(self.s)

    # AM.write() tests

    def test_write_should_work_as_expected(self):
        # Simulate writing a packet
        sreqpkt = FMReqPacket((10, 20, 30, 5, [101, 102, 103, 104, 105]))
        result = self.am.write(sreqpkt, 123)

        # Return code of tos.AM.write() should be True:
        assert result is True

        # Arguments passed to Serial.write() should be as expected:
        assert len(self.s.write_history) == 1
        payload, timeout = self.s.write_history[0]
        assert isinstance(payload, tos.ActiveMessage)
        assert timeout is None

    def test_write_with_timeout_should_work_as_expected(self):
        # Simulate writing a packet
        sreqpkt = FMReqPacket((10, 20, 30, 5, [101, 102, 103, 104, 105]))
        result = self.am.write(sreqpkt, 123, timeout=5.0)

        # Return code of tos.AM.write() should be True:
        assert result is True

        # Arguments passed to Serial.write() should be as expected:
        assert len(self.s.write_history) == 1
        payload, timeout = self.s.write_history[0]
        assert isinstance(payload, tos.ActiveMessage)
        assert timeout == 5.0

